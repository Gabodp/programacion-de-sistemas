#include <math.h>
#include "point.h"


double calcDistance(struct Point a, struct Point b){
	
	
	double x = pow(a.x - b.x,2);
	double y = pow(a.y - b.y,2);
	double z = pow(a.z - b.z,2);
	
	return sqrt(x+y+z);
}
