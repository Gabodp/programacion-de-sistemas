program : point.o main.o
	gcc  main.o point.o -o program -I. -lm
	
point.o : point.c point.h
	gcc -c point.c -I. -lm
main.o : main.c
	gcc -c main.c -I. -lm

clean :
	rm program main.o point.o

